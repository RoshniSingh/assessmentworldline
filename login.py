import json
import sys

def login():
    if len(sys.argv) == 3:
       
        username = sys.argv[1]
        password = sys.argv[2]
    else:
        
        username = "default_username"
        password = "default_password"

    
    with open('registration_details.json', 'r') as file:
        registration_details = json.load(file)

    
    if (username == registration_details['username'] and
            password == registration_details['password']):
        print("Login successful. Welcome, {}!".format(registration_details['first_name']))
    else:
        print("Invalid username or password.")

if __name__ == "__main__":
    login()
