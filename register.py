import sys
import json

def register():
    if sys.stdin.isatty():
        
        first_name = input("Enter your first name: ")
        last_name = input("Enter your last name: ")
        username = input("Enter your username: ")
        password = input("Enter your password: ")
    else:
        
        first_name = "Roshni"  
        last_name = "Singh"
        username = "roshniSingh"
        password = "asdfg"

    
    registration_details = {
        "first_name": first_name,
        "last_name": last_name,
        "username": username,
        "password": password
    }


    with open('registration_details.json', 'w') as file:
        json.dump(registration_details, file)

    print("Registration successful.")

if __name__ == "__main__":
    register()
